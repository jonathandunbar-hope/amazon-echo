/**
 * Created by Jhon on 10/28/2015.
 */
var chai = require('chai');
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var expect = chai.expect;

var MongoDatabase = require('../MongoDatabase');
var DatabaseConnector = require('../Databaseconnector');


describe("Tests da tests", function(){
    it("This should work", function () {
        expect(true).to.equal(true);
    })

    it("Have works synchronously", function () {
       expect([]).to.have.length(0);
    });
});


//change this to which ever database model we're using
//var database = new DatabaseConnector.TempDB();
var database = new MongoDatabase.MongoDatabase();


describe("Tests connection to a mongodb database representing the This Day In History data", function(){
    it("Database exists", function(){
        console.log(database);
        expect(database).to.exist;
    });


    it("Test that database does not contain entries for strings that are not dates", function(){
        expect(database.getDay("blah")).to.be.rejected;
    });
});


describe("Tests that we can set and retrieve information from the database.", function(){
    var may_1_1996 = "Launch of Surveyor 1, the first US spacecraft to land on an extraterrestrial body.";
    var may_1_1933 = "The National Theatre of Greece is founded.";
    var dateEvents;

    before(function(){
        dateEvents = [];
        dateEvents.push(new DatabaseConnector.DateEvent("1996", may_1_1996));
        dateEvents.push(new DatabaseConnector.DateEvent("1933", may_1_1933));
        //database.dropDatabase();
    });

    it("There are no errors thrown while setting day", function(){
        expect(database.setDay("May 1", dateEvents)).to.not.be.rejected;
    });



    it("No errors are thrown on read", function () {
        database.setDay("May 1", dateEvents).then(function(){
            expect(database.getDay("May 1", dateEvents)).to.eventually.have.length(2);
            expect(database.getDay("May 1", dateEvents)).to.equal(dateEvents);
        });
    });



    it("We can delete our history collection", function() {
        database.setDay("May 1", dateEvents)
            .then(function () {
                database.dropDatabase()
                    .then(function (success) {
                        return success
                    },
                    function(error){
                        expect(error).to.be.undefined;
                    }).to.eventually.be.true;
        });
    });
    it("We can receive data for a specific date and year", function(){
        database.setDay("May 1", dateEvents).then(function(){
            expect(database.getDayInYear("May 1", "1996")).to.eventually.equal(may_1_1996);
        });
    });
});

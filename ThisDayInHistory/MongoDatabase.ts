/**
 * Created by Jhon on 10/25/2015.
 */
/// <reference path="../typings/tsd.d.ts" />

import DatabaseConnector = require('./DatabaseConnector');

import {DateEvent} from './DatabaseConnector';
import {Schema} from "mongoose";
import {Model} from "mongoose";
import {Promise} from "es6-promise";
import {Query} from "mongoose";

import {WikiDataFetcher} from "./DataFetcher";

console.log(DatabaseConnector);

import mongoose = require('mongoose');
import {Mongoose} from "mongoose";
import {InvalidDayError} from "./DatabaseConnector";
import {InvalidYearError} from "./DatabaseConnector";

export class MongoDatabase implements DatabaseConnector.DatabaseInterface {
    dbSchema:Schema = new Schema({
        date: String,
        events: Array
        //events: [this.eventsSchema]
    });

    private modelName = "historymodel";
    historyModel;

    private conn:any;

    wikidatafetcher: WikiDataFetcher;


    constructor(){
        this.conn =  mongoose.connect('mongodb://localhost:27017/');
        //this.historyModel = mongoose.model("historyModel", {type: Schema.Types.ObjectId, ref: 'eventSchema'});

        this.historyModel = mongoose.model(this.modelName, this.dbSchema);

        this.wikidatafetcher = new WikiDataFetcher();
    }

    setDay(dateString:string, dateEventsArray:DateEvent[]):Promise<boolean> {
        // .save()
        var aDate = new this.historyModel({date: dateString, events: dateEventsArray});

        return new Promise<boolean>((resolve, reject) => {
            aDate.save(function (err, res) {
                if (err){
                    console.log("Here's an error " + err);
                    reject(err);
                }
                resolve(true);
            });
        });
    }

    getDay(dateString:string):Promise<Array<DateEvent>> {
        var database: MongoDatabase = this;

        return new Promise<Array<DateEvent>>((resolve, reject) => {
            if (!DatabaseConnector.testDay(dateString)){
                reject(new InvalidDayError(dateString));
            }
            var results = this.historyModel.find({date: dateString}, function callback(err, res){
                if (err) {
                    console.log(err);
                    reject(err);
                }
                if (res && res.length > 0){
                    resolve(res[0].events);
                }else{
                    // call datafetcher
                        // then,
                        // setDay for that new stuff

                    database.wikidatafetcher.fetchData(dateString).then(
                        function(resultDateEvents: Array<DateEvent>){
                            database.setDay(dateString, resultDateEvents).then(
                                function(result){
                                    // result is boolean true
                                    if(result){
                                        resolve(resultDateEvents); // send array back up
                                    }else{
                                        reject(false); // ????
                                    }
                                },
                                function(error){
                                    reject(error);
                                }
                            )
                        },
                        function(error){
                            reject(error); //?
                        }
                    );

                }


            });

        });
    }


    getDayInYear(dateString:string, year:string):Promise<string> {
        // .find()

        var database = this;

        return new Promise<string>((resolve, reject) => {
            if (!DatabaseConnector.testDay(dateString)){
                reject(new InvalidDayError(dateString));
            }
            if (!DatabaseConnector.testYear(year)){
                reject(new InvalidYearError(year));
            }
            var getDayPromise = database.getDay(dateString);

            getDayPromise.then(function(events){

                var eventstring = "";

                for (var i in events){
                    if (events[i].year === year){
                        if (eventstring.length > 0){ eventstring += " "; }
                        eventstring += events[i].event;
                    }
                }

                console.log("who let the dogs out: ", eventstring);
                resolve(eventstring);
            });

        });

    }

    dropDatabase():Promise<boolean> {
        //console.log(this.conn);
        var modelName = this.modelName;
        return new Promise<boolean>((resolve, reject) => {
            mongoose.connection.collections[modelName + "s"].drop(function(err){
                if(err){
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        });

    }
}
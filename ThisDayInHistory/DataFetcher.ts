/// <reference path="../typings/tsd.d.ts" />

import {DateEvent} from "./DatabaseConnector";
import https = require('https');
import {Promise} from "es6-promise";


/**
 * Created by Jhon on 11/21/2015.
 *
 * Grabs data for a day from Wikipedia.
 */
export interface DataFetcher {
    fetchData(day:string): Promise<Array<DateEvent>>;

}

export class WikiDataFetcher implements DataFetcher {

    /**
     * Makes a query to Wikipedia for a certain date's data.
     * Returns a promise that resolves the list of DateEvents gotten from Wikipedia.
     * @param string day
     * @returns {Promise}<Array<DateEvents>>
     */
    fetchData(day:string):Promise<Array<DateEvent>> {
        var data = '';
        var thisClass:WikiDataFetcher = this;

        return new Promise<Array<DateEvent>>((resolve, reject) => {
            https.get(thisClass.makeUrl(day), function(res){
                console.log("statusCode: ", res.statusCode);
                console.log("headers: ", res.headers);
                res.on('data', function(d) {
                    data = data + d;
                    //if(buffer.byteLength())
                    process.stdout.write(d);
                });

                res.on('end', function(){
                    //console.log(data);


                    resolve(thisClass.parseData(thisClass.getExtract(data)));

                    //resolve(data);
                });
            }).on('error', function(e){
                reject(e);
            });
        });
    }

    private makeUrl(title:string):string {
        return 'https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&titles=' + title + '&redirects=true';
    }

    private lineRegex = /^<li>(.*)<\/li>$/;
    private dashSplit = " – ";
    private ulSplit = /<ul>((?:(?:<li>.*\s*<\/li>)\s*)*)<\/ul>/;

    /**
     * Converts the extract from the json object into an array of DateEvents.
     * @param extract
     * @returns {Array<DateEvent>}
     */
    private parseData(extract:string):Array<DateEvent>{
        var parsedData:Array<string> = this.ulSplit.exec(extract)[1].split(/\r?\n/);
        var dateEvents:Array<DateEvent> = [];

        for(var i in parsedData){
            var line:string = parsedData[i];
            if(line.match(this.lineRegex)){ //not match with ul
                var values = this.lineRegex.exec(line)[1].split(this.dashSplit);
                try{
                    dateEvents.push(new DateEvent(values[0], values[1]));
                } catch (e){
                    console.log(e);
                }
            }
        }


        return dateEvents;
    }

    /**
     * Returns the extract string containing the pages data.
     * @param data
     * @returns {string}
     */
    private getExtract(data:string):string{
        var pages:any = JSON.parse(data).query.pages;
        var extract:string = '';
        for(var i in pages){
            console.log(i);
            if(pages.hasOwnProperty(i)){
                if(pages[i].extract){
                    extract = pages[i].extract;
                    break;
                }
            }
        }
        return extract;
    }
}
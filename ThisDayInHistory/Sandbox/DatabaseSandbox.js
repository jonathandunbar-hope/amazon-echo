/**
 * Created by Jhon on 10/30/2015.
 *
 * This file is used a blank sandbox to try things.
 */

var mongoose = require('mongoose');
var DataFetcher = require('../DataFetcher');
var WikiDataFetcher = new DataFetcher.WikiDataFetcher();
var MongoDatabase = require('../MongoDatabase');
var DatabaseConnector = require('../DatabaseConnector');
var database = new MongoDatabase.MongoDatabase();
//call the sandbox suite here
//TestTempDB();

//TestDataFetcher();
//AnotherTestRealDB();
TestInvalidDayError();


function TestRealDB(){


    var may_1_1996 = "Launch of Surveyor 1 the first US spacecraft to land on an extraterrestrial body.";
    var may_1_1933 = "The National Theatre of Greece is founded.";
    var dateEvents = [];

    dateEvents.push(new DatabaseConnector.DateEvent("1996", may_1_1996));
    dateEvents.push(new DatabaseConnector.DateEvent("1933", may_1_1933));

    try {
        database.setDay("May 1", dateEvents);
        database.setDay("June 8", dateEvents);
    } catch(e){
        console.log("ay we got problems");
        console.log(e.message);
    }


    try {
        var eventsPromise = database.getDay("May 1");
        console.log("events in sandbox 1");
        eventsPromise.then(
            function(events){
                console.log("test 1 within sandbox get: ", events);
                //trying to wait for drop here. then test 2 should return nothing.
                database.dropDatabase();
            }
        );
        //console.log(events);
    } catch(e) {
        console.log(e.message);
    }

    // this is beating some other stuff to the punch, i think
    //database.dropDatabase();

    try {
        var eventsPromise = database.getDay("May 1");
        console.log("events in sandbox 2");
        eventsPromise.then(
            function(events){
                console.log("test 2 within sandbox: ", events);
            }
        );
        //console.log(events);
    } catch(e) {
        console.log(e.message);
    }

}

function TestDataFetcher(){
    WikiDataFetcher.fetchData('October 3')
        .then(function(data){
            console.log(data);
        });
}

function AnotherTestRealDB(){
    var may_1_1996 = "Launch of Surveyor 1 the first US spacecraft to land on an extraterrestrial body.";
    var may_1_1933 = "The National Theatre of Greece is founded.";
    var dateEvents = [];

    dateEvents.push(new DatabaseConnector.DateEvent("1996", may_1_1996));
    dateEvents.push(new DatabaseConnector.DateEvent("1933", may_1_1933));

    try {
        var eventsPromise = database.getDay("May 1");
        console.log("what will happen???");
        eventsPromise.then(
            function(events){
                console.log("test 1 within sandbox get: ", events);
            }
        );
        //console.log(events);
    } catch(e) {
        console.log(e.message);
    }

}

function TestDrop(){
    database.dropDatabase();
}

function TestInvalidDayError(){
    database.getDay("blah").then(function (events){}, function(error){
       console.log("error", error);
    });
}
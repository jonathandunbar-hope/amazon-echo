import DatabaseConnector = require('./DateEvent');
import {DateEvent} from './DateEvent';
/**
 * Created by Jhon on 11/8/2015.
 *
 * This interface abstracts retrieving data from wherever it is stored.
 */
export interface ServerInterface {    //this interface serves as our connector to the database
    /**
     * Takes a date as input and fetches an array of  'DateEvent' objects for the requested day.
     * The array is passed as a parameter to the callback.
     * @param day
     * @param callback
     */
    getDay(day:string, callback:(events:Array<DateEvent>) => void): void;

    /**
     * Takes a day and year and fetches a string containing the event for that combination.
     * The string is passed as a parameter to the callback.
     * @param day
     * @param year
     * @param callback
     */
    getDayInYear(day:string, year:string, callback:(eventString:string) => void):void;
}


/**
 * This class can be used when dummy data is need for ServerInterface.
 */
class DummyConnector implements ServerInterface {

    getDay(day:string, callback:(events:Array<DateEvent>) => void):void {
        var event1 = 'MLK Birthday';
        var event2 = 'Declaration of Independence was signed';
        var dummyEvents = [];
        dummyEvents.push(event1);
        dummyEvents.push(event2);

        callback(dummyEvents);
    }

    getDayInYear(day:string, year:string, callback:(eventString:string) => void):void {  //day and year as input for an event
        // returns a string 'event'
        var event4 = 'MLK Birthday';
        var eventsArray = [];
        eventsArray.push(event4);
    }
}
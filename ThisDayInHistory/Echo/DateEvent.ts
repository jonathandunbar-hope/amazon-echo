/**
 * Since everything for lambda needs to be in one level, we had to create a copy of DateEvent for this folder. 
 */


class InvalidDayError implements Error {
    public name:string = "InvalidDayError";
    message:string;

    constructor(message: string) {
        this.message = message + ' is not a valid day.';
        if (typeof console !== 'undefined') {
            console.log('Creating ' + this.name + ' "' + message + '"');
        }
    }


    toString() {
        return this.name + ': ' + this.message;
    }

}

class InvalidYearError implements Error {
    public name:string = "InvalidDayError";
    message:string;

    constructor(message: string) {
        this.message = message + ' is not a valid year.';
        if (typeof console !== 'undefined') {
            console.log('Creating ' + this.name + ' "' + message + '"');
        }
    }


    toString() {
        return this.name + ': ' + this.message;
    }

}


export function testYear(year:string):boolean {
    var yearRegex = /^\d{1,4}(?:\sBC)?$/;
    if(year.match(yearRegex)){
        return true;
    } else {
        return false;
    }
}


export class DateEvent {
    private _year:string;

    get year():string {
        return this._year;
    }

    set year(newYear:string) {
        if (testYear(newYear)) {
            this._year = newYear;
        } else {
            throw new InvalidYearError(newYear);
        }
    }

    event:string; //any error checking?

    constructor(year:string, event:string) {
        //is this using the constructors, need to check
        this.year = year;
        this.event = event;
    }
}


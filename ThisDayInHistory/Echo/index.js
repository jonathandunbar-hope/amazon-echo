/**
 * Created by Jhon on 9/17/2015.
 */
var connection = require('./APIServer');
var db = new connection.APIServer();

/**
 * App ID for the skill
 */
var APP_ID = 'amzn1.echo-sdk-ams.app.8445d551-af66-4485-964f-45efb5852428'; //where do we get this from?

/**
 * The AlexaSkill prototype and helper functions
 */
var AlexaSkill = require('AlexaSkill');


/**
 * This Day In History is a child of AlexaSkill.
 *
 */
var TDIH = function () {
    AlexaSkill.call(this, APP_ID);
};

TDIH.prototype = Object.create(AlexaSkill.prototype);

TDIH.prototype.constructor = TDIH;

TDIH.prototype.eventHandlers.onSessionStarted = function (sessionStartedRequest, session) {
    console.log("TDIH onSessionStarted requestId: " + sessionStartedRequest.requestId
        + ", sessionId: " + session.sessionId);
    // any initialization logic goes here
};

TDIH.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    console.log("TDIH onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
    var speechOutput = "TDIH";
    response.ask(speechOutput);
};


TDIH.prototype.intentHandlers = {
    // register custom intent handlers
    BlankIntent: function (intent, session, response) {
        console.log('BlankIntent');
        response.tell("I don't know what you want.");
    },
    GetDayIntent: function (intent, session, response) {
        console.log('GetDayIntent');
        var dateSlot = intent.slots.Day;
        console.log('date slot', dateSlot);
        var dateObject = new Date(dateSlot.value);
        console.log("date object", dateObject);
        console.log("typeof date object", typeof dateObject);
        if(dateObject){
            //response.tellWithCard("day is " + dateObject.getMonth() + " " + dateObject.getDate(), "TDIH", "TDIH");
            var day =  monthNames[dateObject.getMonth()] + " " + dateObject.getDate();
            db.getDay(day, function(events){
                if(events!=null){
                    if(events.length>0){
                        console.log(events);
                        var index = getRandomIntInclusive(0, events.length);
                        console.log('random index', index);
                        var dateEvent = events[index];
                        console.log('dateEvent', dateEvent);
                        var year = dateEvent.year;
                        console.log('year', year);
                        var event = dateEvent.event;
                        console.log('event', event);
                        var responseText = "On "+ day + "In " + year + " " + event;
                        response.tellWithCard(responseText, day + ", " + year, responseText);
                    }
                    else{
                        response.tell("No events for the requested date");
                    }
                }
                else{
                    response.tell("Something went wrong.. Please try again.");
                }
            });
        } else{
            response.tell('GetDayIntent invalid input');
        }
    },
    GetDayAndYearIntent: function(intent, session, response){
        console.log('GetDayAndYearIntent');
        var dayRequested = intent.slots.Day;
        var dateObject = new Date(dayRequested.value);
        var year = intent.slots.Year.value;
        var day =  monthNames[dateObject.getMonth()] + " " + dateObject.getDate();
        if(year){
            db.getDayInYear(day, year, function(event){
                var responseText = "On "+ day +" in "+ year + event;
                response.tellWithCard(responseText, day + ", " + year, responseText);
            });
        } else{
            response.tell('GetDayAndYearIntent invalid input');
        }
    }
};


// Returns a random integer between min (included) and max (included)
// Using Math.round() will give you a non-uniform distribution!
//from Mozilla Developer Network
function getRandomIntInclusive(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];



// Create the handler that responds to the Alexa Request.
exports.handler = function (event, context) {
    // Create an instance of the TDIH skill.
    var object = new TDIH();
    object.execute(event, context);
};


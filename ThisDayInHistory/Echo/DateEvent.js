/**
 * Since everything for lambda needs to be in one level, we had to create a copy of DateEvent for this folder.
 */
var InvalidDayError = (function () {
    function InvalidDayError(message) {
        this.name = "InvalidDayError";
        this.message = message + ' is not a valid day.';
        if (typeof console !== 'undefined') {
            console.log('Creating ' + this.name + ' "' + message + '"');
        }
    }
    InvalidDayError.prototype.toString = function () {
        return this.name + ': ' + this.message;
    };
    return InvalidDayError;
})();
var InvalidYearError = (function () {
    function InvalidYearError(message) {
        this.name = "InvalidDayError";
        this.message = message + ' is not a valid year.';
        if (typeof console !== 'undefined') {
            console.log('Creating ' + this.name + ' "' + message + '"');
        }
    }
    InvalidYearError.prototype.toString = function () {
        return this.name + ': ' + this.message;
    };
    return InvalidYearError;
})();
function testYear(year) {
    var yearRegex = /^\d{1,4}(?:\sBC)?$/;
    if (year.match(yearRegex)) {
        return true;
    }
    else {
        return false;
    }
}
exports.testYear = testYear;
var DateEvent = (function () {
    function DateEvent(year, event) {
        //is this using the constructors, need to check
        this.year = year;
        this.event = event;
    }
    Object.defineProperty(DateEvent.prototype, "year", {
        get: function () {
            return this._year;
        },
        set: function (newYear) {
            if (testYear(newYear)) {
                this._year = newYear;
            }
            else {
                throw new InvalidYearError(newYear);
            }
        },
        enumerable: true,
        configurable: true
    });
    return DateEvent;
})();
exports.DateEvent = DateEvent;
//# sourceMappingURL=DateEvent.js.map
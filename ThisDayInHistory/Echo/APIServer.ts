/**
 * Created by Jhon on 11/8/2015.
 */
import ServerConnector = require('./ServerConnector');
import {DateEvent} from "./DateEvent";
import http = require('http');

/**
 * This class retreives data from our express server running on EC2.
 */
export class APIServer implements ServerConnector.ServerInterface {

    ip = "http://54.209.124.221/";

    getDay(day:string, callback:(events:Array<DateEvent>) => void):void {

        var data = "";
        var apiServer = this;


        http.get(apiServer.ip + "getDay/" + day, function (res) {
            res.on("data", function (d) {
                data = data + d;
                console.log("dataReceive: ", data);
            });

            res.on("end", function () {
                console.log("dataEnd: ", data);
                var events:Array<DateEvent> = [];
                var jsonObjects:Array<any> = JSON.parse(data);


                // iterate over array
                for (var i in jsonObjects) {
                    var object = jsonObjects[i];
                    // turn each thing into a real DateEvent object and add it to events
                    console.log('event', object['event'])
                    events.push(new DateEvent(object['_year'], object['event']));
                }
                // resolve on the array of dateevents
                callback(events);
            });
        }).on("error", function (e) {
            console.log("error: ", e);
            callback(null);
        });
    }

    getDayInYear(day:string, year:string, callback:(eventString:string) => void):void {
        var data = "";
        var apiServer = this;

        http.get(apiServer.ip + "getDayInYear/" + day + "/" + year, function (res) {
            res.on("data", function (d) {
                data = data + d;
            });

            res.on("end", function () {
                callback(data);
            });
        }).on("error", function (e) {
            callback(null);
        });
    }
}
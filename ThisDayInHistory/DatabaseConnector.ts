import {Promise} from "es6-promise";

/**
 * Allows access to a database to set/get information.
 * All methods return Promise that resolve on the requested data.
 */
export interface DatabaseInterface {

    dropDatabase(): Promise<boolean>;

    setDay(day:string, dataEvents:DateEvent[]): Promise<boolean>;

    getDay(day:string): Promise<Array<DateEvent>>;

    getDayInYear(day:string, year:string): Promise<string>;
}

/**
 * An error for represeting an invalid day.
 */
export class InvalidDayError implements Error {
    public name:string = "InvalidDayError";
    message:string;

    constructor(message: string) {
        this.message = message + ' is not a valid day.';
        if (typeof console !== 'undefined') {
            console.log('Creating ' + this.name + ' "' + message + '"');
        }
    }


    toString() {
        return this.name + ': ' + this.message;
    }

}

/**
 * An error for represeting an invalid year.
 */
export class InvalidYearError implements Error {
    public name:string = "InvalidDayError";
    message:string;

    constructor(message: string) {
        this.message = message + ' is not a valid year.';
        if (typeof console !== 'undefined') {
            console.log('Creating ' + this.name + ' "' + message + '"');
        }
    }


    toString() {
        return this.name + ': ' + this.message;
    }

}

/**
 * Tests the validity of a string representing a day.
 * @param day
 * @returns {boolean}
 */
export function testDay(day:string):boolean {
    var keyRegex = /^\w+\s\d{1,2}$/;
    if (day.match(keyRegex)) {
        return true;
    } else {
        return false;
    }
}

/**
 * Tests the validity of a string representing a year.
 * @param year
 * @returns {boolean}
 */
export function testYear(year:string):boolean {
    var yearRegex = /^\d{1,4}(?:\sBC)?$/;
    if(year.match(yearRegex)){
        return true;
    } else {
        return false;
    }
}

/**
 * This class represents an event occurring on a date-year combination. e.g. July 4, 1776
 */
export class DateEvent {
    private _year:string;

    get year():string {
        return this._year;
    }

    set year(newYear:string) {
        if (testYear(newYear)) {
            this._year = newYear;
        } else {
            throw new InvalidYearError(newYear);
        }
    }

    event:string; //any error checking?

    constructor(year:string, event:string) {
        //is this using the constructors, need to check
        this.year = year;
        this.event = event;
    }
}


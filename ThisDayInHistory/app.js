/**
 * Created by Palmer D'Orazio on 12/1/2015.
 *
 * This script creates a basic express server which returns json data for getDay and getDayInYear.
 */


var express = require('express');
var app = express();

var MongoDatabase = require('./MongoDatabase');
var database = new MongoDatabase.MongoDatabase();

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect

    res.setHeader('Access-Control-Allow-Origin', 'http://www.pdorazio.com'); //this will be pdorazio.com
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get('/getDayInYear/:date/:year', function (req, res){
    var dateString = req.params.date;
    var yearString = req.params.year;

    console.log("date string", dateString);
    console.log("year string", yearString);

    database.getDayInYear(dateString, yearString).then(
        function(eventString){
            console.log("eventString in getDayInYear ", eventString);
            res.send(eventString); // ??
        },
        function(error){
            res.send("some sort of error: ", error);
        }
    );
});

app.get('/getDay/:date', function (req, res){
   var dateString = req.params.date;

    console.log("date string", dateString);

    database.getDay(dateString).then(
        function(eventsArray){
            var allEvents = JSON.stringify(eventsArray);
            console.log("length of all events array: ", allEvents.length);
            res.send(allEvents);
        },
        function(error){
            console.log("error in getDay: ", error);
            res.send("some sort of error: ", error);
        }
    )
});

app.get('/getSingleEvent/:date', function(req, res){
    var dateString = req.params.date;

    database.getDay(dateString).then(
        function(eventsArray){

            var randIndex = getRandomIntInclusive(0, eventsArray.length);
            var singleEvent = eventsArray[randIndex];

            var eventString = singleEvent.year + ": " + singleEvent.event;

            res.send(eventString);
        },
        function(error){
            console.log("Error retrieving single event: ", error);
            res.send("Error retrieving event; please try again.");
        }
    )

});

app.get('/', function (req, res) {
    res.send('Methods are getDay(date) and getDayInYear(date, year)');
});

var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('This Day In History listening at http://%s:%s', host, port);
});


// Returns a random integer between min (included) and max (included)
// Using Math.round() will give you a non-uniform distribution!
//from Mozilla Developer Network
function getRandomIntInclusive(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
